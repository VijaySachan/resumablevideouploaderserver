const express = require('express')
const http = require('http')
const socket = require('socket.io')
const fs = require('fs')
const exec = require('child_process').exec
const util = require('util')
var ffmpeg = require('ffmpeg');
const port = 3001

const app = express() 

const server = http.Server(app)
const io = socket(server)

app.use(express.static('public'))
app.use(express.static('Video'))
server.listen(port, () => {
	console.log(`App running on port ${port}`)
}) 

var Files = {};
// socket
io.on('connection', (socket)=>{
	console.log("Inside io.on(connection)");
	socket.emit('connected', 'connected')

	// socket.on('connected', (data) =>{
	// 	console.log(data)
	// })

	socket.on('Start', function (data) { 
		//data contains the variables that we passed through in the html file
	    var Name = data['Name'];
	    Files[Name] = {  //Create a new Entry in The Files Variable
	        FileSize : data['Size'],
	        Data   : "",
	        Downloaded : 0
	    }
	    var Place = 0;
	    try{
	        var Stat = fs.statSync('Temp/' +  Name);
	        if(Stat.isFile())
	        {
	            Files[Name]['Downloaded'] = Stat.size;
				Place = Stat.size / 524288;
				console.log("File is already present ");
	        }
	    }
	    catch(er){} //It's a New File
	    fs.open("Temp/" + Name, "a",0755,  function(err, fd){
	        if(err)
	        {
			  console.log(err);
			//   Files[Name]['Handler'] = fd; 
            //   socket.emit('MoreData', { 'Place' : 0, Percent : 0 });
	        }
	        else
	        {
				console.log("Downloaded "+Files[Name]['Downloaded'] +" client file size "+Files[Name]['FileSize'])
               if(Files[Name]['Downloaded'] >= Files[Name]['FileSize']){
				  console.log("File is already uploaded ")
				  socket.emit('Done', {});
			   }else{


	            Files[Name]['Handler'] = fd; 
	            //We store the file handler so we can write to it later
				socket.emit('MoreData', { 'Place' : Place, Percent : 0 });
			}
	        }
	    });
	});

	socket.on('Upload', function (data){
	    var Name = data['Name'];
	    Files[Name]['Downloaded'] += data['Data'].length;
	    Files[Name]['Data'] += data['Data'];
	    if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
	    {
			console.log("Fully uploaded")
	        fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
				//Get Thumbnail Here
				console.log("writing to file "+err)
				console.log("100 % "+Writen);
				socket.emit('Done', {});
// 	            try {
// 					new ffmpeg("Temp/" + Name, function (err, video) {
// 						if (!err) {
// 							console.log('The video is ready to be processed');
// 							video.fnExtractFrameToJPG('Temp', {
// 								frame_rate : 1,
// 			number : 5,
// 			file_name : 'my_frame_%t_%s'
// 			, keep_pixel_aspect_ratio	: true		// Mantain the original pixel video aspect ratio
//   , keep_aspect_ratio		: true		// Mantain the original aspect ratio
//   , padding_color			: 'black'	// Padding color
// 							}, function (error, files) {
// 								if (!error){
// 									console.log('Frames: ' + files);
// 									socket.emit('Done', {'Image' : 'Temp/' + Name + '.jpg'});
// 						// socket.emit('Done', {});
						
// 								}
// 									else{
// 										console.log("Unable to create thumbnail "+error)
// 									}
// 							});
// 						} else {
// 							console.log('Error: ' + err);
// 						}
// 					});
// 				} catch (e) {
// 					console.log(e.code);
// 					console.log(e.msg);
// 				}

	                    /*This ffmpeg command will generate one thumbnail at the 1:30 
	                    mark, and save it to the Video/ folder with a .jpg file type.*/
	                    // exec("ffmpeg -i Temp/" + Name  + " -ss 00:05 -r 1 -an -vframes 1 -f mjpeg Temp/" + Name  + ".jpg", function(err){
						// 	console.log(err);
							
						// });
						
	                });
	            // });
	        // });
	    }
	    else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
	        fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary',   function(err, Writen){
	            Files[Name]['Data'] = ""; //Reset The Buffer
	            var Place = Files[Name]['Downloaded'] / 524288;
				var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
				console.log(Percent+" % "+Writen);
	            socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
	        });
	    }
	    else
	    {
	        var Place = Files[Name]['Downloaded'] / 524288;
			var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
			console.log(Math.round(Percent)+" % ");
	        socket.emit('MoreData', { 'Place' : Place, 'Percent' :  Percent});
	    }
	});

	socket.on('MoreData', function (data){
		var percentUploaded=data['Percent']
		
	    UpdateBar(percentUploaded);
	    var Place = data['Place'] * 524288; //The Next Blocks Starting Position
	    var NewFile; //The Variable that will hold the new Block of Data
	    if(SelectedFile.webkitSlice)
	        NewFile = SelectedFile.webkitSlice(Place, Place + Math.min(524288, (SelectedFile.size-Place)));
	    else
	        NewFile = SelectedFile.mozSlice(Place, Place + Math.min(524288, (SelectedFile.size-Place)));
	    FReader.readAsBinaryString(NewFile);
	});

	socket.on('WriteData', function (data){
	    var Name = data['Name'];
	    
	    fs.write(Files[Name]['Handler'], Files[Name]['Data'],  null, 'Binary',  function(err, Writen){
			Files[Name]['Data'] = ""; //Reset The Buffer
			var Place = Files[Name]['Downloaded'] / 524288;
			var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
			console.log("WriteData "+Percent+" % "+Writen);
			
		});
	    
	});


	// function UpdateBar(percent){
	//     document.getElementById('ProgressBar').style.width = percent + '%';
	//     document.getElementById('percent').innerHTML = (Math.round(percent*100)/100) + '%';
	//     var MBDone = Math.round(((percent/100.0) * SelectedFile.size) / 1048576);
	//     document.getElementById('MB').innerHTML = MBDone;
	// }

})